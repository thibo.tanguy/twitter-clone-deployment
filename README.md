## Twitter clone deployment

Description
Script de déploiement Docker Compose pour déployer l'application twitter-clone (API .NET + Serveur Web Nginx avec l'application React).

Déploiement

Pré-requis
Vous devez avoir Docker et Docker compose installés sur votre machine et avoir authentifié votre compte Gitlab avec les droits d'accès au container registry du groupe ASCI.

Installation
Rendez vous dans le répertoire courant du fichier docker-compose.yaml fournit et effectuer la commande :

```
docker-compose up
```

Documentation
Le fichier de déploiement docker-compose.yaml permet de démarrer 2 containers, un pour l'API .NET et un pour le serveur web Nginx avec l'application Angular compilé.
Ce script va récupérer les images des projets sur les repositories Gitlab des 2 projet et lancer les 2 containers avec les images pré-configurés.

```yaml
version: '3'
services:
  database:
    image: twitter-clone-database
    restart: always
    networks:
      - twitter-clone-network

  api:
    image: twitter-clone-backend
    restart: always
    depends_on:
      - database
    networks:
      - twitter-clone-network

  app:
    image: twitter-clone-frontend
    restart: always
    depends_on:
      - api
    ports:
      - "80:80"
    networks:
      - twitter-clone-network

networks:
  twitter-clone-network:
```